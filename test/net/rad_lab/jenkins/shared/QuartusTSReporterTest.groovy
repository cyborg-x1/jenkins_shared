package net.rad_lab.jenkins.shared

import spock.lang.Specification

class QuartusTSReporterTest extends Specification {
    def "parseQuartusTS"() {
        setup:
        Io.pipe = new TestIo()

        when:
        def reporter = new QuartusTSReporter("build/resources/test/quartus_build/log")
        reporter.writeReport("build/reports")
        then:
        6 == 6
    }
}
