package net.rad_lab.jenkins.shared
import java.io.File
import groovy.transform.Field
import static groovy.io.FileType.FILES

class TestIo {
    class FileSearch {
        String glob
        String excludes
    }

    def println(obj) {
        System.out.println(obj)
    }

    String readFile(String path, String encoding="utf-8") {
        return new File(path).getText(encoding)
    }

    def writeFile(String path, String text, String encoding="utf-8") {
        def file = new File(path)
        file.write(text, encoding)
    }

    static def findFiles(FileSearch s) {
        def glob = s.glob
        def excludes = s.excludes
        def files = []
        def dir = new File("./")

        if (glob != null) {
            glob = glob.replace(".","\\.").replace("*", ".*")
        }

        if (excludes != null) {
            excludes = excludes.replace(".","\\.").replace("*", ".*")
        }

        dir.eachFileRecurse (FILES) { file ->
            def path = file.toString()

            def take = false

            if ((glob == null) || (path =~ glob)) {
                take = true
            }

            if ((excludes != null) && (path =~ excludes)){
                take = false
            }

            if (take) {
                files.add(file)
            }
        }
        return files
    }

    static def findFiles(Map m)
    { findFiles m as FileSearch }
}
