package net.rad_lab.jenkins.shared
import com.cloudbees.groovy.cps.NonCPS
import static groovy.io.FileType.FILES

class LocalJobs
{
    @NonCPS
    def createLocalJobs(String directory)
    {
        def dir = new File(directory);
        def files = [];
        dir.traverse(type: FILES, maxDepth: 1)
        {
            if (it.toString().endsWith("rldsl.yml"))
            {
                files.add(it)
                Io.pipe.println(it)
            }
        }
        Io.pipe.println(files)
    }
}
