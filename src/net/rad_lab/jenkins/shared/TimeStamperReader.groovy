package net.rad_lab.jenkins.shared

import groovy.xml.XmlUtil

class TimeStamperReader {
    private String suiteName
    private def details

    TimeStamperReader(directory)
    {
        this.details = []
        def globPattern = directory + "/*-timestamper.xml"
        def files = Io.pipe.findFiles(glob: globPattern)

        files.each { file ->
            def fileContent = Io.pipe.readFile(file.toString()).toString()
            this.details.add(new XmlSlurper().parseText(fileContent))
        }
    }

    def getDetails()
    {
        return this.details
    }
}
