package net.rad_lab.jenkins.shared
import com.cloudbees.groovy.cps.NonCPS


class QuartusTSReporter {
    static final def cmds = ["quartus_fit", "quartus_sh", "quartus_map", "quartus_sta", "quartus_cpf"]
    private def details = []
    private def categories = []

    static final def message_template = "^( *)TYPE \\(([0-9]*)\\): (.*)\$"
    static final def knownMessageTypes = ["Info", "Warning", "Critical Warning", "Error"]


    QuartusTSReporter(directory, reportName="Quartus.Synthesis.TS") {
        this.details = new TimeStamperReader(directory).getDetails()
    }

    def writeReport(outDirectory)
    {
        this.cmds.each { cmd ->
            this.details.each { detail ->
                if (detail.cmd =~ "/" + cmd + " ") {
                    print("----" + cmd + "-----")
                    int lineNumber = 0
                    detail.stdout.toString().split("\n").each { line ->
                        lineNumber++

                        this.knownMessageTypes.each { t ->
                            def currentPattern = ~message_template.replace("TYPE", t)
                            def matcher = currentPattern.matcher(line)
                            if (matcher.matches())
                            {
                                def indented = (matcher.group(1).size() > 0)
                                def number = matcher.group(2)
                                def text = matcher.group(3)

                                if(indented)
                                {
                                    indented = " -"
                                }
                                else
                                {
                                    indented = ""
                                }
                                println(indented + lineNumber.toString() + " " + t + " " + number)
                            }
                        }
                    }
                }
            }
        }
    }
}
